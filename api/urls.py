"""Project URL Configuration"""

from django.conf.urls import include
from django.urls import path

urlpatterns = [
    path('gateway/', include('gateway.urls', namespace='gateway')),
    path('locker/', include('locker.urls', namespace='locker')),
    path('proxy/', include('proxy.urls', namespace='proxy'))
]
