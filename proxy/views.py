import logging
from urllib.parse import urljoin

import requests
from django.conf import settings
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST

logger = logging.getLogger(__name__)


@api_view(['POST'])
def submit_payment(request):
    try:
        # submit payment request to proxy
        resp = requests.post(urljoin(settings.LOCKER_URL, 'submit-payment/'), json=request.data)
        return Response(resp.json(), status=resp.status_code)
    except Exception:
        logger.exception('Failed to submit payment request to locker.', exc_info=True)
        return Response({"error": "Failed to submit payment"}, status=HTTP_400_BAD_REQUEST)
