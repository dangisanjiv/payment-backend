from django.urls import path

from . import views

app_name = 'locker'

urlpatterns = [
    path('submit-payment/', views.submit_payment, name='submit-payment'),
]
