# Payment Backend

# Installation
1. Install mysql-client: `$ sudo apt install -y libmysqlclient-dev`
2. Install Packages: `$ pipenv install`
3. Copy `.env.example` file to `.env`
4. Generate keys: `$ pipenv run python manage.py keygenerate`
5. Configure environment variable on `.env` file
6. Run migration: `$ pipenv run python manage.py migrate`
7. Start dev server: `$ pipenv run python manage.py runserver`