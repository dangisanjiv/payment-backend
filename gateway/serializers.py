from rest_framework.serializers import ModelSerializer

from gateway.models import Payment


class PaymentSerializer(ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'


class PaymentDataSerializer(ModelSerializer):
    class Meta:
        model = Payment
        fields = ('amount', 'remarks', 'card_id')
