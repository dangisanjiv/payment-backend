import logging

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED

from gateway.serializers import PaymentSerializer, PaymentDataSerializer

logger = logging.getLogger(__name__)


@api_view(['POST'])
def process_payment(request):
    # serialize and store payment details
    serialized_pmt = PaymentDataSerializer(data=request.data)
    serialized_pmt.is_valid(raise_exception=True)
    payment = serialized_pmt.save()

    # I guess nothing to do from here for now.

    logger.info('Payment success')
    return Response(PaymentSerializer(payment).data, status=HTTP_201_CREATED)
