from django.urls import path

from . import views

app_name = 'gateway'

urlpatterns = [
    path('process-payment/', views.process_payment, name='process-payment'),
]
