from uuid import uuid4

from django.db import models
from django.utils.timezone import now


class Payment(models.Model):
    uuid = models.UUIDField(default=uuid4, unique=True)
    amount = models.DecimalField(max_digits=16, decimal_places=6)
    remarks = models.CharField(max_length=100, null=True, blank=True)
    card_id = models.IntegerField()
    created_at = models.DateTimeField(default=now)
