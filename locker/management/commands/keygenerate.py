import nacl
from django.core.management.base import BaseCommand
from nacl import utils
from nacl.encoding import Base64Encoder

from nacl.public import PrivateKey
from nacl.secret import SecretBox


class Command(BaseCommand):
    help = 'Generates new secret key, public key, and private key'

    def handle(self, *args, **options):
        # generate
        secret_key = nacl.utils.random(SecretBox.KEY_SIZE)
        private_key = PrivateKey.generate()
        public_key = private_key.public_key

        # encode
        _secret_key = Base64Encoder.encode(secret_key).decode()
        _private_key = private_key.encode(Base64Encoder).decode()
        _public_key = public_key.encode(Base64Encoder).decode()

        print('\nSECRET_KEY={}'.format(_secret_key))
        print('PRIVATE_KEY={}'.format(_private_key))
        print('PUBLIC_KEY={}'.format(_public_key))
        print('\nPaste these to .env file\n')
