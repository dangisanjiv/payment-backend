from uuid import uuid4

from django.db import models
from django.utils.timezone import now


class Card(models.Model):
    uuid = models.UUIDField(default=uuid4, unique=True)
    name = models.CharField(max_length=64)
    number = models.CharField(max_length=100)
    expiry = models.CharField(max_length=12)
    cvv = models.CharField(max_length=8)
    created_at = models.DateTimeField(default=now)
