from base64 import b64decode, b64encode

from nacl.encoding import Base64Encoder
from nacl.public import SealedBox, PublicKey, PrivateKey
from nacl.secret import SecretBox

from api import settings

_secret_key = None
_public_key = None
_private_key = None


def get_secret_key():
    global _secret_key
    if _secret_key is None:
        _secret_key = b64decode(settings.SECRET_KEY)
    return _secret_key


def get_public_key():
    global _public_key
    if _public_key is None:
        _public_key = PublicKey(settings.PUBLIC_KEY, Base64Encoder)
    return _public_key


def get_private_key():
    global _private_key
    if _private_key is None:
        _private_key = PrivateKey(settings.PRIVATE_KEY, Base64Encoder)
        _private_key.public_key = get_public_key()
    return _private_key


def encrypt(data):
    return b64encode((SealedBox(get_public_key())).encrypt(data)).decode('utf-8')


def decrypt(payload):
    return (SealedBox(get_private_key())).decrypt(b64decode(payload))


def tokenize(data):
    return b64encode((SecretBox(get_secret_key())).encrypt(data)).decode('utf-8')


def de_tokenize(payload):
    return (SecretBox(get_secret_key())).decrypt(b64decode(payload))
