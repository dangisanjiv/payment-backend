from rest_framework.fields import CharField, DecimalField
from rest_framework.serializers import ModelSerializer, Serializer

from locker.models import Card


class CardSerializer(ModelSerializer):
    class Meta:
        model = Card
        fields = ('name', 'number', 'expiry', 'cvv')


class PaymentDataSerializer(Serializer):
    token = CharField(max_length=10000, required=True)
    amount = DecimalField(max_digits=16, decimal_places=6, required=True)
    remarks = CharField(max_length=100, required=False, allow_null=True, allow_blank=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
