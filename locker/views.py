import json
import logging
from urllib.parse import urljoin

import requests
from django.conf import settings
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_200_OK

from locker.models import Card
from locker.serializers import CardSerializer, PaymentDataSerializer
from locker.utils import decrypt, tokenize, de_tokenize

logger = logging.getLogger(__name__)


@api_view(['GET'])
def public_key(request):
    # return public key from settings
    return Response({"public_key": settings.PUBLIC_KEY})


@api_view(['POST'])
def store_card(request):
    payload = request.data.get('payload')
    try:
        # decrypt request payload
        card_data = decrypt(payload).decode('utf-8')
    except Exception:
        logger.exception('Failed to decrypt card data.', exc_info=True)
        return Response({'error': 'Invalid payload'}, status=HTTP_400_BAD_REQUEST)

    # serializer and validate decrypted card data
    serialized_card = CardSerializer(data=json.loads(card_data))
    serialized_card.is_valid(raise_exception=True)
    card = serialized_card.save()

    try:
        # create card token using uuid
        token = tokenize(str(card.uuid).encode())
    except Exception:
        logger.exception('Failed to create token.', exc_info=True)
        return Response({'error': 'Failed to create token'}, status=HTTP_400_BAD_REQUEST)

    return Response({'token': token}, status=HTTP_201_CREATED)


@api_view(['POST'])
def submit_payment(request):
    # serialize and validate post data
    serialized_pd = PaymentDataSerializer(data=request.data)
    serialized_pd.is_valid(raise_exception=True)
    payment_data = serialized_pd.data

    try:
        # de-tokenize card token
        uuid = de_tokenize(payment_data.get('token')).decode('utf-8')
    except Exception:
        logger.exception('Failed to de-tokenize token.', exc_info=True)
        return Response({'error': 'Invalid payload'}, status=HTTP_400_BAD_REQUEST)

    # fetch card
    card = Card.objects.get(uuid=uuid)

    try:
        # submit payment request to gateway
        resp = requests.post(urljoin(settings.GATEWAY_URL, 'process-payment/'), json={
            'card_id': card.id,
            'name': card.name,
            'number': card.number,
            'expiry': card.expiry,
            'cvv': card.cvv,
            'amount': payment_data.get('amount'),
            'remarks': payment_data.get('remarks'),
        })

        if resp.status_code == 201:
            return Response({'message': 'Payment submitted'}, status=HTTP_200_OK)
        else:
            logger.error('Failed to submit payment; status={}'.format(resp.status_code))
    except Exception:
        logger.exception('Failed to de-tokenize token.', exc_info=True)

    return Response({"error": "Failed to submit payment"}, status=HTTP_400_BAD_REQUEST)
