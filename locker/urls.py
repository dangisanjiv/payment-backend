from django.urls import path

from . import views

app_name = 'locker'

urlpatterns = [
    path('public-key/', views.public_key, name='public-key'),
    path('store-card/', views.store_card, name='store-card'),
    path('submit-payment/', views.submit_payment, name='submit-payment'),
]
